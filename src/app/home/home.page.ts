import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  choice: string = 'en';
  constructor(private translate: TranslateService) { }

  ngOnInit() {}

  changeUser(params: string) {
    this.translate.use(params);
    this.choice = this.translate.currentLang;
  }

}
